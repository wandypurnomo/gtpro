<?php

use App\Models\User;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Wandxx\Support\Constants\ActiveStatus;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = User::firstOrCreate(['email' => 'superuser@gtpro.com'], ['name' => 'superuser', 'password' => 'password', 'email' => 'superuser@gtpro.com', 'status' => ActiveStatus::ACTIVE]);
        $role = Role::whereName('super user')->first();
        $user->syncRoles($role);
    }
}
