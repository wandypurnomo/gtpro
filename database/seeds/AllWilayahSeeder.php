<?php

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class AllWilayahSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $path = storage_path("indonesia.sql");

        Schema::dropIfExists("villages");
        Schema::dropIfExists("districts");
        Schema::dropIfExists("regencies");
        Schema::dropIfExists("provinces");

        DB::unprepared(file_get_contents($path));

        $this->command->info('Indonesia table seeded!');

        Model::reguard();
    }
}
