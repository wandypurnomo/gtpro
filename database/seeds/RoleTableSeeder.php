<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Role::firstOrCreate(['name' => 'super user'], ['name' => 'super user']);
        Role::firstOrCreate(['name' => 'admin'], ['name' => 'admin']);
        Role::firstOrCreate(['name' => 'agent'], ['name' => 'agent']);
        Role::firstOrCreate(['name' => 'agent_warehouse'], ['name' => 'agent_warehouse']);
        Role::firstOrCreate(['name' => 'driver'], ['name' => 'driver']);
        Role::firstOrCreate(['name' => 'courier'], ['name' => 'courier']);
    }
}
