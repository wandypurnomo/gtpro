<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Wandxx\Support\Constants\ActiveStatus;

class CreateAgentCourierTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('agent_courier', function (Blueprint $table) {
            $table->uuid('courier_id');
            $table->uuid('agent_id');
            $table->unsignedTinyInteger('status')->default(ActiveStatus::ACTIVE);

            $table->foreign('courier_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');
            $table->foreign('agent_id')
                ->references('id')
                ->on('agents')
                ->onDelete('cascade');

            $table->primary(['courier_id', 'agent_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('agent_courier');
    }
}
