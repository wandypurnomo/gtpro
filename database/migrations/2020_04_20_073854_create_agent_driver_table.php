<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAgentDriverTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('agent_driver', function (Blueprint $table) {
            $table->uuid('driver_id');
            $table->uuid('agent_id');
            $table->unsignedTinyInteger('status');

            $table->foreign('driver_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');
            $table->foreign('agent_id')
                ->references('id')
                ->on('agents')
                ->onDelete('cascade');

            $table->primary(['driver_id', 'agent_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('agent_driver');
    }
}
