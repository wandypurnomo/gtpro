<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePickupFleetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pick_up_fleet', function (Blueprint $table) {
            $table->uuid('pick_up_id');
            $table->uuid('fleet_id');
            $table->foreign('pick_up_id')
                ->references('id')
                ->on('pick_ups')
                ->onDelete('cascade');
            $table->foreign('fleet_id')
                ->references('id')
                ->on('fleets')
                ->onDelete('cascade');
            $table->primary(['pick_up_id', 'fleet_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pick_up_fleet');
    }
}
