<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->uuid('agent_id');
            $table->string('code')->unique();
            $table->string('warehouse_origin');
            $table->string('warehouse_destination');
            $table->unsignedTinyInteger('goods_type');
            $table->double('weight');
            $table->string('notes');
            $table->string('sender_name');
            $table->string('sender_phone');
            $table->string('sender_phone2')->nullable();
            $table->text('sender_address');
            $table->string('receiver_name');
            $table->string('receiver_phone');
            $table->string('receiver_phone2')->nullable();
            $table->text('receiver_address');
            $table->unsignedTinyInteger('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
