<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::group(['middleware' => 'guest'], function () {
    Route::get('login', 'UserController@login')->name('login');
    Route::post('login', 'UserController@doLogin')->name('login.post');
});

Route::group(['prefix' => 'region', 'as' => 'region.'], function () {
    Route::get('villageAjax', 'RegionController@villageAjax');
    Route::get('villageAjax/{id}', 'RegionController@villageAjaxSingle');
});

Route::group(['middleware' => 'auth', 'prefix' => 'adm'], function () {
    Route::get('/', 'UserController@dashboard')->name('dashboard');
    Route::get('logout', 'UserController@logout')->name('logout');

    Route::group(['prefix' => 'usermanager', 'as' => 'usermanager.'], function () {
        Route::get('/', 'UserController@index')->name('index');
        Route::get('add', 'UserController@add')->name('add');
        Route::post('add', 'UserController@store')->name('add.post');
        Route::get('edit/{userId}', 'UserController@edit')->name('edit');
        Route::post('edit/{userId}', 'UserController@update')->name('edit.post');
        Route::get('delete/{userId}','UserController@delete')->name('delete');
    });

    Route::group(['prefix' => 'agentmanager', 'as' => 'agentmanager.'], function () {
        Route::get('/', 'AgentController@index')->name('index');
        Route::get('add', 'AgentController@add')->name('add');
        Route::post('add', 'AgentController@store')->name('add.post');
        Route::get('edit/{agentId}', 'AgentController@edit')->name('edit');
        Route::post('edit/{agentId}', 'AgentController@update')->name('edit.post');
        Route::get('delete/{agentId}', 'AgentController@delete')->name('delete');
        Route::get('add-user/{agentId}', 'AgentController@addUser')->name('add-user');
        Route::post('add-user/{agentId}', 'AgentController@storeUser')->name('add-user.post');
        Route::get('edit-user/{agentId}/{userId}', 'AgentController@editUser')->name('edit-user');
        Route::post('edit-user/{userId}', 'AgentController@updateUser')->name('edit-user.post');
        Route::get('add-driver/{agentId}', 'AgentController@addDriver')->name('add-driver');
        Route::post('add-driver/{agentId}', 'AgentController@storeDriver')->name('add-driver.post');
        Route::get('edit-driver/{agentId}/{userId}', 'AgentController@editDriver')->name('edit-driver');
        Route::get('add-courier/{agentId}', 'AgentController@addCourier')->name('add-courier');
        Route::post('add-courier/{agentId}', 'AgentController@storeCourier')->name('add-courier.post');
        Route::get('edit-courier/{agentId}/{userId}', 'AgentController@editCourier')->name('edit-courier');

        Route::get('show/{agentId}', 'AgentController@show')->name('show');
    });

    Route::group(['prefix' => 'drivermanager', 'as' => 'drivermanager.'], function () {
        Route::get('/', 'DriverController@index')->name('index');
    });
});
