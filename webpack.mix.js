const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */
mix.styles([
    'resources/css/bootstrap.min.css',
    'resources/css/font-awesome.min.css',
    'resources/css/ionicons.min.css',
    'resources/css/select2.min.css',
    'resources/css/AdminLTE.min.css',
    'resources/css/_all-skins.min.css',
    'resources/css/alt/AdminLTE-select2.min.css',
    'resources/css/toastr.min.css'
], 'public/css/all.css')
    .scripts([
        'resources/js/jquery.min.js',
        'resources/js/bootstrap.min.js',
        'resources/js/jquery.slimscroll.min.js',
        'resources/js/fastclick.js',
        'resources/js/adminlte.min.js',
        'resources/js/toastr.min.js',
        'resources/js/select2.full.min.js',
        'resources/js/scripts.js'
    ], 'public/js/all.js')
    .copyDirectory('resources/fonts','public/fonts')
    .copyDirectory('resources/img','public/img');
