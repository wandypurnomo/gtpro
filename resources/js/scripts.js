$(document).ready(function () {
    $('.sidebar-menu').tree();
    $('.s2').select2({
        width: '100%'
    });

    var villageSelector = $('.s2-village-ajax');
    var villageSelector2 = villageSelector;
    villageSelector.select2({
        width: '100%',
        ajax: {
            url: '/region/villageAjax',
            dataType: 'json',
            data: function (params) {
                return {
                    search: params.term,
                    page: params.page || 1
                }
            },
            processResults: function (data, params) {
                params.page = params.page || 1;

                return {
                    results: data.results,
                    pagination: {
                        more: (params.page * 10) < data.count_filtered
                    }
                };
            }
        }
    });
});

// if(typeof $selectedVillage !== undefined){
//     $.ajax({
//         type:'GET',
//         url:'/region/villageAjax/'+$selectedVillage
//     }).then(function(data){
//         var option = new Option(data.text, data.id.toString(), true, true);
//         villageSelector2.append(option).trigger('change');
//
//         // manually trigger the `select2:select` event
//         villageSelector2.trigger({
//             type: 'select2:select',
//             params: {
//                 data: data
//             }
//         });
//     });
// }

toastr.options = {
    "closeButton": true,
    "debug": false,
    "newestOnTop": false,
    "progressBar": true,
    "positionClass": "toast-bottom-right",
    "preventDuplicates": false,
    "onclick": null,
    "showDuration": "300",
    "hideDuration": "1000",
    "timeOut": "5000",
    "extendedTimeOut": "1000",
    "showEasing": "swing",
    "hideEasing": "linear",
    "showMethod": "fadeIn",
    "hideMethod": "fadeOut"
};

