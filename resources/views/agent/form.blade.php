<div class="form-group">
    {!! Form::label('code','Code',['class'=>'control-label col-sm-4']) !!}
    <div class="col-sm-8">
        {!! Form::text('code',null,['class'=>'form-control']) !!}
    </div>
</div>

<div class="form-group">
    {!! Form::label('name','Name',['class'=>'control-label col-sm-4']) !!}
    <div class="col-sm-8">
        {!! Form::text('name',null,['class'=>'form-control']) !!}
    </div>
</div>

<div class="form-group">
    {!! Form::label('address','Address',['class'=>'control-label col-sm-4']) !!}
    <div class="col-sm-8">
        {!! Form::textarea('address',null,['class'=>'form-control','rows'=>'3']) !!}
    </div>
</div>

<div class="form-group">
    {!! Form::label('phone','Phone',['class'=>'control-label col-sm-4']) !!}
    <div class="col-sm-8">
        {!! Form::text('phone',null,['class'=>'form-control']) !!}
    </div>
</div>

<div class="form-group">
    {!! Form::label('phone2','Phone 2',['class'=>'control-label col-sm-4']) !!}
    <div class="col-sm-8">
        {!! Form::text('phone2',null,['class'=>'form-control']) !!}
    </div>
</div>

<div class="form-group">
    {!! Form::label('lat','Latitude',['class'=>'control-label col-sm-4']) !!}
    <div class="col-sm-8">
        {!! Form::text('lat',null,['class'=>'form-control']) !!}
    </div>
</div>

<div class="form-group">
    {!! Form::label('lng','Longitude',['class'=>'control-label col-sm-4']) !!}
    <div class="col-sm-8">
        {!! Form::text('lng',null,['class'=>'form-control']) !!}
    </div>
</div>

<div class="form-group">
    {!! Form::label('type','Type',['class'=>'control-label col-sm-4']) !!}
    <div class="col-sm-8">
        {!! Form::select('type',$types,null,['class'=>'form-control','placeholder' => 'Select Agent Type']) !!}
    </div>
</div>

<div class="form-group">
    {!! Form::label('village_id','Region',['class'=>'control-label col-sm-4']) !!}
    <div class="col-sm-8">
        {!! Form::select('village_id',[],null,['class'=>'form-control s2-village-ajax','placeholder' => 'Select Region']) !!}
    </div>
</div>

<div class="form-group">
    <div class="col-sm-12">
        <div class="text-right">
            <button type="submit" class="btn btn-success">Save</button>
        </div>
    </div>
</div>