@extends('layouts.base')
@section('title','GTPro | User Manager | Add')
@section('page_header_title','Add User')
@push('contents')
    <div class="row">
        <div class="col-sm-6">
            <div class="box box-solid">
                <div class="box-body">
                    {!! Form::open(['class'=>'form-horizontal','autocomplete'=>'off']) !!}
                    {!! Form::hidden('roles[]',config('gtpro.courier_id')) !!}
                    @include('agent.form_user')
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>


@endpush