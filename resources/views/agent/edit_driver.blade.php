@extends('layouts.base')
@section('title','GTPro | User Manager | Edit')
@section('page_header_title','Edit User')
@push('contents')
    <div class="row">
        <div class="col-sm-6">
            <div class="box box-solid">
                <div class="box-body">
                    {!! Form::model($user,['class'=>'form-horizontal','autocomplete'=>'off','route' => ['agentmanager.edit-user.post','userId'=>$user->id]]) !!}
                    {!! Form::hidden('roles[]',config('gtpro.driver_id')) !!}
                    @include('agent.form_user')
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>


@endpush