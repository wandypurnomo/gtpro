@extends('layouts.base')
@section('title','GTPro | Agent Manager')
@section('page_header_title')
    Agent Manager &nbsp; <a href="{{ route('agentmanager.add') }}" class="btn btn-success btn-xs"><i
                class="fa fa-plus"></i> Add</a>
@stop

@push('contents')
    <div class="box box-solid">
        <div class="box-body">
            <table class="table">
                <thead>
                <tr>
                    <th>Action</th>
                    <th>Code</th>
                    <th>Name</th>
                    <th>Address</th>
                    <th>Phone</th>
                    <th>Type</th>
                </tr>
                </thead>
                <tbody>
                @forelse($agents as $agent)
                    <tr>
                        <td>
                            <a href="{{ route('agentmanager.edit',['agentId' => $agent->id]) }}"
                               class="btn btn-success btn-xs"><i class="fa fa-pencil"></i></a>
                            <a onclick="return confirm('Delete user?')"
                               href="{{ route('agentmanager.delete',['agentId' => $agent->id]) }}"
                               class="btn btn-success btn-xs"><i class="fa fa-trash"></i></a>
                            <a class="btn btn-success btn-xs"
                               href="{{ route('agentmanager.show',['agentId'=>$agent->id]) }}"><i
                                        class="fa fa-eye"></i></a>
                        </td>
                        <td>{{ $agent->code }}</td>
                        <td>{{ $agent->name }}</td>
                        <td>{{ $agent->address }}</td>
                        <td>{{ $agent->phone }}</td>
                        <td>{{ $agent->type_text }}</td>
                    </tr>
                @empty
                    <tr>
                        <td colspan="6" class="text-center">No Data</td>
                    </tr>
                @endforelse
                </tbody>
            </table>
            <div class="text-right">
                {!! $agents->render() !!}
            </div>
        </div>
    </div>
@endpush