@extends('layouts.base')
@section('title','GTPro | Agent Manager | Edit')
@section('page_header_title','Edit Agent')
@push('contents')
    <div class="row">
        <div class="col-sm-6">
            <div class="box box-solid">
                <div class="box-body">
                    {!! Form::model($agent,['class'=>'form-horizontal','autocomplete'=>'off']) !!}
                    @include('agent.form')
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endpush

@push('heads')
    <script>
        var $selectedVillage = {{ $selected ?? "" }};
    </script>
@endpush