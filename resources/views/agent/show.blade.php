@extends('layouts.base')
@section('title','GTPro | Agent Manager | Show')
@section('page_header_title','Show Agent')
@push('contents')
    <div class="row">
        <div class="col-sm-6">
            <div class="box box-solid">
                <div class="box-body">
                    <div class="form-horizontal">
                        <div class="form-group">
                            {!! Form::label('code','Code',['class'=>'control-label col-sm-4']) !!}
                            <div class="col-sm-8">
                                <div class="form-control-static">{{ $agent->code }}</div>
                            </div>
                        </div>

                        <div class="form-group">
                            {!! Form::label('type','Type',['class'=>'control-label col-sm-4']) !!}
                            <div class="col-sm-8">
                                <div class="form-control-static">{{ \App\Constants\AgentType::label($agent->type) }}</div>
                            </div>
                        </div>

                        <div class="form-group">
                            {!! Form::label('name','Name',['class'=>'control-label col-sm-4']) !!}
                            <div class="col-sm-8">
                                <div class="form-control-static">{{ $agent->name }}</div>
                            </div>
                        </div>

                        <div class="form-group">
                            {!! Form::label('address','Address',['class'=>'control-label col-sm-4']) !!}
                            <div class="col-sm-8">
                                <div class="form-control-static">{{ $agent->address }}</div>
                            </div>
                        </div>

                        <div class="form-group">
                            {!! Form::label('lat','Latitude',['class'=>'control-label col-sm-4']) !!}
                            <div class="col-sm-8">
                                <div class="form-control-static">{{ $agent->lat }}</div>
                            </div>
                        </div>

                        <div class="form-group">
                            {!! Form::label('lng','Longitude',['class'=>'control-label col-sm-4']) !!}
                            <div class="col-sm-8">
                                <div class="form-control-static">{{ $agent->lng }}</div>
                            </div>
                        </div>

                        <div class="form-group">
                            {!! Form::label('phone','Phone',['class'=>'control-label col-sm-4']) !!}
                            <div class="col-sm-8">
                                <div class="form-control-static">{{ $agent->phone }}</div>
                            </div>
                        </div>

                        <div class="form-group">
                            {!! Form::label('phone2','Phone 2',['class'=>'control-label col-sm-4']) !!}
                            <div class="col-sm-8">
                                <div class="form-control-static">{{ $agent->phone2 ?? '-' }}</div>
                            </div>
                        </div>

                        <div class="form-group">
                            {!! Form::label('village','Village',['class'=>'control-label col-sm-4']) !!}
                            <div class="col-sm-8">
                                <div class="form-control-static">{{ $agent->village->name ?? '-' }}</div>
                            </div>
                        </div>

                        <div class="form-group">
                            {!! Form::label('district','District',['class'=>'control-label col-sm-4']) !!}
                            <div class="col-sm-8">
                                <div class="form-control-static">{{ $agent->village->district->name ?? '-' }}</div>
                            </div>
                        </div>

                        <div class="form-group">
                            {!! Form::label('regency','Regency',['class'=>'control-label col-sm-4']) !!}
                            <div class="col-sm-8">
                                <div class="form-control-static">{{ $agent->village->district->regency->name ?? '-' }}</div>
                            </div>
                        </div>

                        <div class="form-group">
                            {!! Form::label('province','Province',['class'=>'control-label col-sm-4']) !!}
                            <div class="col-sm-8">
                                <div class="form-control-static">{{ $agent->village->district->regency->province->name ?? '-' }}</div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <div class="box box-success box-solid">
                <div class="box-header">
                    <h3 class="box-title">User</h3>
                </div>
                <div class="box-body">
                    <table class="table">
                        <thead>
                        <tr>
                            <th>Action</th>
                            <th>Name</th>
                            <th>Email</th>
                        </tr>
                        </thead>
                        <tbody>
                        @forelse($agent->users()->get() as $user)
                            <tr>
                                <td>
                                    <a href="{{ route('agentmanager.edit-user',['userId'=>$user->id,'agentId'=>$agent->id]) }}"
                                       class="btn btn-success btn-xs"><i class="fa fa-pencil"></i></a>
                                    <a onclick="return confirm('Delete user?')"
                                       href="{{ route('usermanager.delete',['userId'=>$user->id]) }}"
                                       class="btn btn-success btn-xs"><i class="fa fa-trash"></i></a>
                                </td>
                                <td>{{ $user->name }}</td>
                                <td>{{ $user->email }}</td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="3" class="text-center">No User</td>
                            </tr>
                        @endforelse
                        </tbody>
                    </table>
                    <div class="text-right">
                        <a href="{{ route('agentmanager.add-user',['agentId'=>$agent->id]) }}"
                           class="btn btn-success">Add User</a>
                    </div>
                </div>
            </div>
        </div>
        @if($agent->type == \App\Constants\AgentType::WAREHOUSE)
            <div class="col-sm-6">
                <div class="box box-success box-solid">
                    <div class="box-header">
                        <h3 class="box-title">Driver</h3>
                    </div>
                    <div class="box-body">
                        <table class="table">
                            <thead>
                            <tr>
                                <th>Action</th>
                                <th>Name</th>
                                <th>Email</th>
                            </tr>
                            </thead>
                            <tbody>
                            @forelse($agent->drivers()->get() as $user)
                                <tr>
                                    <td>
                                        <a href="{{ route('agentmanager.edit-driver',['userId'=>$user->id,'agentId'=>$agent->id]) }}"
                                           class="btn btn-success btn-xs"><i class="fa fa-pencil"></i></a>
                                        <a onclick="return confirm('Delete user?')"
                                           href="{{ route('usermanager.delete',['userId'=>$user->id]) }}"
                                           class="btn btn-success btn-xs"><i class="fa fa-trash"></i></a>
                                    </td>
                                    <td>{{ $user->name }}</td>
                                    <td>{{ $user->email }}</td>
                                </tr>
                            @empty
                                <tr>
                                    <td colspan="3" class="text-center">No User</td>
                                </tr>
                            @endforelse
                            </tbody>
                        </table>
                        <div class="text-right">
                            <a href="{{ route('agentmanager.add-driver',['agentId'=>$agent->id]) }}"
                               class="btn btn-success">Add Driver</a>
                        </div>
                    </div>
                </div>

                <div class="box box-success box-solid">
                    <div class="box-header">
                        <h3 class="box-title">Courier</h3>
                    </div>
                    <div class="box-body">
                        <table class="table">
                            <thead>
                            <tr>
                                <th>Action</th>
                                <th>Name</th>
                                <th>Email</th>
                            </tr>
                            </thead>
                            <tbody>
                            @forelse($agent->couriers()->get() as $user)
                                <tr>
                                    <td>
                                        <a href="{{ route('agentmanager.edit-courier',['userId'=>$user->id,'agentId'=>$agent->id]) }}"
                                           class="btn btn-success btn-xs"><i class="fa fa-pencil"></i></a>
                                        <a onclick="return confirm('Delete user?')"
                                           href="{{ route('usermanager.delete',['userId'=>$user->id]) }}"
                                           class="btn btn-success btn-xs"><i class="fa fa-trash"></i></a>
                                    </td>
                                    <td>{{ $user->name }}</td>
                                    <td>{{ $user->email }}</td>
                                </tr>
                            @empty
                                <tr>
                                    <td colspan="3" class="text-center">No User</td>
                                </tr>
                            @endforelse
                            </tbody>
                        </table>
                        <div class="text-right">
                            <a href="{{ route('agentmanager.add-courier',['agentId'=>$agent->id]) }}"
                               class="btn btn-success">Add Courier</a>
                        </div>
                    </div>
                </div>
            </div>
        @endif
    </div>
@endpush