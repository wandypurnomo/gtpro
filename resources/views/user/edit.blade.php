@extends('layouts.base')
@section('title','GTPro | User Manager | Edit')
@section('page_header_title','Edit User')
@push('contents')
    <div class="row">
        <div class="col-sm-6">
            <div class="box box-solid">
                <div class="box-body">
                    {!! Form::model($user,['class'=>'form-horizontal','autocomplete'=>'off']) !!}
                    @include('user.form')
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>


@endpush