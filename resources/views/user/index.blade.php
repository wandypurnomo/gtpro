@extends('layouts.base')
@section('title','GTPro | User Manager')
@section('page_header_title')
    User Manager &nbsp; <a href="{{ route('usermanager.add') }}" class="btn btn-success btn-xs"><i class="fa fa-plus"></i> Add</a>
@stop

@push('contents')
    <div class="box box-solid">
        <div class="box-body">
            <table class="table">
                <thead>
                <tr>
                    <th>Action</th>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Status</th>
                    <th>Roles</th>
                </tr>
                </thead>
                <tbody>
                @forelse($users as $user)
                    <tr>
                        <td>
                            @if(auth()->id() != $user->id)
                            <a href="{{ route('usermanager.edit',['userId' => $user->id]) }}" class="bt btn-success btn-xs"><i class="fa fa-pencil"></i></a>
                            <a onclick="return confirm('Delete user?')" href="{{ route('usermanager.delete',['userId' => $user->id]) }}" class="bt btn-success btn-xs"><i class="fa fa-trash"></i></a>
                            @endif
                        </td>
                        <td>{{ $user->name }}</td>
                        <td>{{ $user->email }}</td>
                        <td>{{ $user->status_text }}</td>
                        <td>{{ implode(', ',$user->roles->pluck('name')->toArray()) }}</td>
                    </tr>
                @empty
                    <tr>
                        <td colspan="5" class="text-center">No Data</td>
                    </tr>
                @endforelse
                </tbody>
            </table>
            <div class="text-right">
                {!! $users->render() !!}
            </div>
        </div>
    </div>
@endpush