<div class="form-group">
    {!! Form::label('name','Name',['class'=>'control-label col-sm-4']) !!}
    <div class="col-sm-8">
        {!! Form::text('name',null,['class'=>'form-control']) !!}
    </div>
</div>

<div class="form-group">
    {!! Form::label('email','Email',['class'=>'control-label col-sm-4']) !!}
    <div class="col-sm-8">
        {!! Form::text('email',null,['class'=>'form-control']) !!}
    </div>
</div>

<div class="form-group">
    {!! Form::label('password','Password',['class'=>'control-label col-sm-4']) !!}
    <div class="col-sm-8">
        {!! Form::password('password',['class'=>'form-control']) !!}
    </div>
</div>

<div class="form-group">
    {!! Form::label('password_confirmation','Confirm Password',['class'=>'control-label col-sm-4']) !!}
    <div class="col-sm-8">
        {!! Form::password('password_confirmation',['class'=>'form-control']) !!}
    </div>
</div>

<div class="form-group">
    {!! Form::label('status','Status',['class'=>'control-label col-sm-4']) !!}
    <div class="col-sm-8">
        <div style="margin-top: 10px">
            <div><label>{!! Form::checkbox('status',1,isset($user) ? $user->status == 1:false) !!} active</label></div>
        </div>
    </div>
</div>

<div class="form-group">
    {!! Form::label('roles','Roles',['class'=>'control-label col-sm-4']) !!}
    <div class="col-sm-8">
        {!! Form::select('roles[]',$roles->pluck('name','id'),isset($user) ? $user->roles->first()->id:null,['class'=>'form-control','placeholder'=>'Select Role']) !!}
    </div>
</div>
<div class="form-group">
    <div class="col-sm-12">
        <div class="text-right">
            <button type="submit" class="btn btn-success">Save</button>
        </div>
    </div>
</div>