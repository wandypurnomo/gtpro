@extends('layouts.base')
@section('title','GTPro | User Manager | Add')
@section('page_header_title','Add User')
@push('contents')
    <div class="row">
        <div class="col-sm-6">
            <div class="box box-solid">
                <div class="box-body">
                    {!! Form::open(['class'=>'form-horizontal','autocomplete'=>'off']) !!}
                    @include('user.form')
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>


@endpush