@extends('layouts.base')
@section('title','GTPro | Driver Manager')
@section('page_header_title','Driver Manager')
@push('contents')
    <div class="row">
        <div class="col-sm-12">
            <div class="box box-success">
                <div class="box-body">
                    <table class="table">
                        <thead>
                        <tr>
                            <th>Action</th>
                            <th>Name</th>
                            <th>Email</th>
                        </tr>
                        </thead>
                        <tbody>
                        @forelse($drivers as $user)
                            <tr>
                                <td>
                                    <a href="{{ route('agentmanager.edit-driver',['userId'=>$user->id,'agentId'=>$agent->id]) }}"
                                       class="btn btn-success btn-xs"><i class="fa fa-pencil"></i></a>
                                    <a onclick="return confirm('Delete user?')"
                                       href="{{ route('usermanager.delete',['userId'=>$user->id]) }}"
                                       class="btn btn-success btn-xs"><i class="fa fa-trash"></i></a>
                                </td>
                                <td>{{ $user->name }}</td>
                                <td>{{ $user->email }}</td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="3" class="text-center">No User</td>
                            </tr>
                        @endforelse
                        </tbody>
                    </table>
                    <div class="text-right">
                        <a href="{{ route('agentmanager.add-driver',['agentId'=>$agent->id]) }}"
                           class="btn btn-success">Add Driver</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endpush