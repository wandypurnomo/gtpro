<?php


namespace App\Http\Requests\Agent;


use App\Constants\PickupStatus;
use Dingo\Api\Http\FormRequest;
use Wandxx\Support\Interfaces\DefaultRequestInterface;

class RequestPickUpRequest extends FormRequest implements DefaultRequestInterface
{

    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'pick_up_preference' => 'required|numeric',
            'fleet_requirement' => 'required|numeric'
        ];
    }

    public function data(): array
    {
        $this->merge(['status' => PickUpStatus::REQUEST]);
        $only = [
            'pick_up_preference',
            'fleet_requirement',
            'status'
        ];

        return $this->only($only);
    }
}