<?php

namespace App\Http\Requests\Agent;

use Illuminate\Foundation\Http\FormRequest;
use Wandxx\Support\Interfaces\DefaultRequestInterface;

class StoreAgentRequest extends FormRequest implements DefaultRequestInterface
{

    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'code' => 'required|unique:agents,code',
            'name' => 'required',
            'address' => 'required',
            'phone' => 'required',
            'lat' => 'required',
            'lng' => 'required',
            'type' => 'required|numeric'
        ];
    }

    public function data(): array
    {
        return $this->only([
            'code', 'name', 'address', 'phone',
            'phone2', 'district_id', 'lat', 'lng',
            'type'
        ]);
    }
}
