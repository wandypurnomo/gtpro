<?php


namespace App\Http\Requests\Api;


use Dingo\Api\Http\FormRequest;
use Wandxx\Support\Interfaces\DefaultRequestInterface;

class LoginRequest extends FormRequest implements DefaultRequestInterface
{

    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'email' => 'required|email',
            'password' => 'required'
        ];
    }

    public function data(): array
    {
        return $this->only(['email', 'password']);
    }
}