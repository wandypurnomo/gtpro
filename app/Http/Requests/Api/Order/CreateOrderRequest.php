<?php


namespace App\Http\Requests\Api\Order;


use Dingo\Api\Http\FormRequest;
use Wandxx\Support\Interfaces\DefaultRequestInterface;

class CreateOrderRequest extends FormRequest implements DefaultRequestInterface
{

    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'warehouse_origin' => 'required',
            'warehouse_destination' => 'required',
            'goods_type' => 'required|numeric',
            'weight' => 'required|numeric',
            'sender_name' => 'required|max:160',
            'sender_phone' => 'required:max:160',
            'sender_address' => 'required',
            'receiver_name' => 'required|max:160',
            'receiver_phone' => 'required:max:160',
            'receiver_address' => 'required',
        ];
    }

    public function data(): array
    {
        $only = [
            'warehouse_origin', 'warehouse_destination', 'goods_type', 'weight', 'notes',
            'sender_name', 'sender_phone', 'sender_phone2', 'sender_address',
            'receiver_name', 'receiver_phone', 'receiver_phone2', 'receiver_address',
            'status'
        ];

        return $this->only($only);
    }
}