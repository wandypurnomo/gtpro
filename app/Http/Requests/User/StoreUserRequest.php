<?php

namespace App\Http\Requests\User;

use Illuminate\Foundation\Http\FormRequest;
use Wandxx\Support\Interfaces\DefaultRequestInterface;

class StoreUserRequest extends FormRequest implements DefaultRequestInterface
{

    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'name' => 'required',
            'password' => 'required|confirmed',
            'email' => 'required|email|unique:users,email'
        ];
    }

    public function data(): array
    {
        $this->merge(['status' => $this->filled('status') ? 1 : 0]);
        $only = ['name', 'password', 'email', 'status','roles'];
        return $this->only($only);
    }
}
