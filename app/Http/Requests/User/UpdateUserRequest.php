<?php

namespace App\Http\Requests\User;

use Illuminate\Foundation\Http\FormRequest;
use Request;
use Wandxx\Support\Interfaces\DefaultRequestInterface;

class UpdateUserRequest extends FormRequest implements DefaultRequestInterface
{

    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        $id = Request::segment(4);
        return [
            'name' => 'required',
            'email' => 'required|email|unique:users,email,'.$id,
            'password' => 'confirmed'
        ];
    }

    public function data(): array
    {
        $this->merge(['status' => $this->filled('status') ? 1 : 0]);
        $only = ['name', 'password', 'email', 'status', 'roles'];

        if ($this->input('password') == null) {
            $only = ['name', 'email', 'status', 'roles'];
        }

        return $this->only($only);
    }
}
