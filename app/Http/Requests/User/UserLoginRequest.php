<?php

namespace App\Http\Requests\User;

use Illuminate\Foundation\Http\FormRequest;
use Wandxx\Support\Interfaces\DefaultRequestInterface;

class UserLoginRequest extends FormRequest implements DefaultRequestInterface
{

    public function authorize(): bool
    {
        return auth()->guest();
    }

    public function rules(): array
    {
        return [
            'email' => 'required|email',
            'password' => 'required'
        ];
    }

    public function data(): array
    {
        return $this->only(['email', 'password']);
    }

    public function remember(): bool
    {
        return $this->filled('remember');
    }
}
