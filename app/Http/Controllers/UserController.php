<?php

namespace App\Http\Controllers;

use App\Contracts\UserRepositoryContract;
use App\Http\Requests\User\StoreUserRequest;
use App\Http\Requests\User\UpdateUserRequest;
use App\Http\Requests\User\UserLoginRequest;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;
use Spatie\Permission\Models\Role;

class UserController extends Controller
{
    private $_userRepository;

    public function __construct(UserRepositoryContract $_userRepository)
    {
        $this->_userRepository = $_userRepository;
    }

    public function login(): View
    {
        return view('login');
    }

    public function doLogin(UserLoginRequest $request): RedirectResponse
    {
        if (!auth()->attempt($request->data(), $request->remember())) {
            return back()->with(['error' => 'Credential not match.']);
        }

        return redirect()
            ->route('dashboard')
            ->with(['success' => sprintf('Welcome back %s', auth()->user()->name)]);
    }

    public function dashboard(): View
    {
        return view('adm.dashboard');
    }

    public function logout(): RedirectResponse
    {
        auth()->logout();
        return redirect()->route('login')->with(['success' => 'Logged out succesfully.']);
    }

    public function index(Request $request)
    {
        $users = $this->_userRepository->allUser($request);
        return view('user.index', compact('users'));
    }

    public function add(): View
    {
        $roles = Role::all();
        return view('user.add', compact('roles'));
    }

    public function edit(string $userId): View
    {
        $roles = Role::all();
        $user = $this->_userRepository->show($userId);
        return view('user.edit', compact('roles', 'user'));
    }

    public function show(string $userId)
    {
        $user = $this->_userRepository->show($userId);
        return view('user.show', compact('user'));
    }

    public function store(StoreUserRequest $request): RedirectResponse
    {
        $this->_userRepository->store($request->data());
        return redirect()->route('usermanager.index')->with(['success' => 'User added.']);
    }

    public function update(UpdateUserRequest $request, $userId): RedirectResponse
    {
        $this->_userRepository->update($request->data(), $userId);
        return redirect()->route('usermanager.index')->with(['success' => 'User updated.']);
    }

    public function delete(string $userId): RedirectResponse
    {
        $this->_userRepository->delete($userId);
        return back()->with(['success' => 'User deleted.']);
    }

    public function updateProfile(StoreUserRequest $request): RedirectResponse
    {
        $this->_userRepository->updateProfile($request->data(), auth()->id());
        return back();
    }
}
