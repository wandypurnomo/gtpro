<?php

namespace App\Http\Controllers\Api;

use App\Contracts\AgentRepositoryContract;
use App\Contracts\OrderRepositoryContract;
use App\Contracts\PickUpRepositoryContract;
use App\Http\Controllers\Controller;
use App\Http\Requests\Agent\RequestPickUpRequest;
use App\Http\Requests\Api\Order\CreateOrderRequest;
use App\Transformers\Order\OrderTransformer;
use Dingo\Api\Http\Response;
use Dingo\Api\Routing\Helpers;
use Illuminate\Http\Request;

class AgentController extends Controller
{
    use Helpers;

    private $_agentRepository;
    private $_orderRepository;
    private $_pickUpRepository;

    public function __construct(AgentRepositoryContract $_agentRepository, OrderRepositoryContract $_orderRepository, PickUpRepositoryContract $_pickUpRepository)
    {
        $this->_agentRepository = $_agentRepository;
        $this->_orderRepository = $_orderRepository;
        $this->_pickUpRepository = $_pickUpRepository;
    }

    public function allOrder(Request $request): Response
    {
        $currentUser = $this->auth()->getUser();
        $agent = $this->_agentRepository->showAgentByUser($currentUser);
        $data = $this->_orderRepository->allByAgent($request, $agent);
        return $this->response()->paginator($data, new OrderTransformer());
    }

    public function showOrder(string $orderId): Response
    {
        $order = $this->_orderRepository->show($orderId);
        return $this->response()->item($order, new OrderTransformer());
    }

    public function createOrder(CreateOrderRequest $request): Response
    {
        $currentUser = $this->auth()->getUser();
        $agent = $this->_agentRepository->showAgentByUser($currentUser);
        $data = $this->_orderRepository->create($request->data(), $agent);
        return $this->response()->item($data, new OrderTransformer());
    }

    public function requestPickup(RequestPickUpRequest $request): Response
    {
        $currentUser = $this->auth()->getUser();
        $agent = $this->_agentRepository->showAgentByUser($currentUser);
        $this->_pickUpRepository->requestPickup($request->data(), $agent);
        return $this->response()->created();
    }
}
