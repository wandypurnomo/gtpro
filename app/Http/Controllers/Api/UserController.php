<?php

namespace App\Http\Controllers\Api;

use App\Contracts\UserRepositoryContract;
use App\Http\Controllers\Controller;
use App\Http\Requests\Api\LoginRequest;
use App\Transformers\AuthTransformer;
use App\Transformers\UserTransformer;
use Dingo\Api\Http\Response;
use Dingo\Api\Routing\Helpers;
use JWTAuth;

class UserController extends Controller
{
    use Helpers;

    private $_userRepository;

    public function __construct(UserRepositoryContract $_userRepository)
    {
        $this->_userRepository = $_userRepository;
    }

    public function profile(): Response
    {
        $user = $this->_userRepository->show(auth()->id());
        return $this->response()->item($user, new UserTransformer());
    }

    public function login(LoginRequest $request): Response
    {
        if (!$token = JWTAuth::attempt($request->data())) {
            $this->response()->error('Credential not match.', 401);
        }

        $data = collect(compact('token'));
        return $this->response()->item($data, new AuthTransformer());
    }
}
