<?php

namespace App\Http\Controllers;

use App\Constants\AgentType;
use App\Contracts\AgentRepositoryContract;
use App\Contracts\RegionRepositoryContract;
use App\Contracts\UserRepositoryContract;
use App\Http\Requests\Agent\StoreAgentRequest;
use App\Http\Requests\Agent\UpdateAgentRequest;
use App\Http\Requests\User\StoreUserRequest;
use App\Http\Requests\User\UpdateUserRequest;
use DB;
use Exception;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;
use Wandxx\Support\Constants\ActiveStatus;

class AgentController extends Controller
{
    private $_agentRepository;
    private $_userRepository;
    private $_regionRepository;

    public function __construct(AgentRepositoryContract $_agentRepository, UserRepositoryContract $_userRepository, RegionRepositoryContract $_regionRepository)
    {
        $this->_agentRepository = $_agentRepository;
        $this->_userRepository = $_userRepository;
        $this->_regionRepository = $_regionRepository;
    }

    public function index(Request $request): View
    {
        $agents = $this->_agentRepository->allAgent($request);
        return view('agent.index', compact('agents'));
    }

    public function add(): View
    {

        $types = AgentType::labels();
        return view('agent.add', compact('types'));
    }

    public function store(StoreAgentRequest $request): RedirectResponse
    {
        $this->_agentRepository->store($request->data());
        return redirect()->route('agentmanager.index')->with(['success' => 'Agent added.']);
    }

    public function edit(string $agentId): View
    {
        $agent = $this->_agentRepository->show($agentId);
        $types = AgentType::labels();
        $selected = $agent->village_id;
        return view('agent.edit', compact('agent', 'types', 'selected'));
    }

    public function show(string $agentId): View
    {
        $agent = $this->_agentRepository->show($agentId);
        return view('agent.show', compact('agent'));
    }

    public function update(UpdateAgentRequest $request, $agentId): RedirectResponse
    {
        $this->_agentRepository->update($request->data(), $agentId);
        return redirect()->route('agentmanager.index')->with(['success' => 'Agent updated.']);
    }

    public function delete(string $agentId): RedirectResponse
    {
        $this->_agentRepository->delete($agentId);
        return redirect()->route('agentmanager.index')->with(['success' => 'Agent deleted.']);
    }

    public function addUser(string $agentId): View
    {
        return view('agent.add_user');
    }

    public function editUser(string $agentId, string $userId): View
    {
        $agent = $this->_agentRepository->show($agentId);
        $user = $agent->users()->findOrFail($userId);
        return view('agent.edit_user', compact('user'));
    }

    public function storeUser(StoreUserRequest $request, string $agentId): RedirectResponse
    {
        DB::beginTransaction();

        try {
            $user = $this->_userRepository->store($request->data());
            $agent = $this->_agentRepository->show($agentId);
            $agent->users()->attach($user->id, ['status' => ActiveStatus::ACTIVE]);
            DB::commit();
            return redirect()->route('agentmanager.show', compact('agentId'))->with(['success' => 'User Added']);
        } catch (Exception $e) {
            DB::rollBack();
            return redirect()->route('agentmanager.show', compact('agentId'))->with(['success' => $e->getMessage()]);
        }
    }

    public function updateUser(UpdateUserRequest $request, string $userId): RedirectResponse
    {
        $this->_userRepository->update($request->data(), $userId);
        return back()->with(['success' => 'User Updated']);
    }

    public function addDriver(string $agentId): View
    {
        return view('agent.add_driver');
    }

    public function editDriver(string $agentId, string $userId): View
    {
        $agent = $this->_agentRepository->show($agentId);
        $user = $agent->drivers()->findOrFail($userId);
        return view('agent.edit_driver', compact('user'));
    }

    public function storeDriver(StoreUserRequest $request, string $agentId): RedirectResponse
    {
        DB::beginTransaction();

        try {
            $user = $this->_userRepository->store($request->data());
            $agent = $this->_agentRepository->show($agentId);
            $agent->drivers()->attach($user->id, ['status' => ActiveStatus::ACTIVE]);
            DB::commit();
            return redirect()->route('agentmanager.show', compact('agentId'))->with(['success' => 'User Added']);
        } catch (Exception $e) {
            DB::rollBack();
            return redirect()->route('agentmanager.show', compact('agentId'))->with(['success' => $e->getMessage()]);
        }
    }

    public function addCourier(string $agentId): View
    {
        return view('agent.add_courier');
    }

    public function editCourier(string $agentId, string $userId): View
    {
        $agent = $this->_agentRepository->show($agentId);
        $user = $agent->couriers()->findOrFail($userId);
        return view('agent.edit_courier', compact('user'));
    }

    public function storeCourier(StoreUserRequest $request, string $agentId): RedirectResponse
    {
        DB::beginTransaction();

        try {
            $user = $this->_userRepository->store($request->data());
            $agent = $this->_agentRepository->show($agentId);
            $agent->couriers()->attach($user->id, ['status' => ActiveStatus::ACTIVE]);
            DB::commit();
            return redirect()->route('agentmanager.show', compact('agentId'))->with(['success' => 'User Added']);
        } catch (Exception $e) {
            DB::rollBack();
            return redirect()->route('agentmanager.show', compact('agentId'))->with(['success' => $e->getMessage()]);
        }
    }

}
