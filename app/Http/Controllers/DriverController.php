<?php


namespace App\Http\Controllers;


use App\Contracts\AgentRepositoryContract;
use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;

class DriverController extends Controller
{
    private $_agentRepository;
    private $_agent;

    public function __construct(AgentRepositoryContract $_agentRepository)
    {
        $this->_agentRepository = $_agentRepository;
        $this->middleware(function ($request, $next) {
            $this->_agent = $this->_agentRepository->showAgentByUser(auth()->user());
            return $next($request);
        });
    }

    public function index(Request $request): View
    {
        $drivers = $this->_agent->drivers()->latest()->paginate(10);
        $agent = $this->_agent;
        return view('driver.index', compact('drivers', 'agent'));
    }
}