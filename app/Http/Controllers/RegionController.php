<?php

namespace App\Http\Controllers;

use App\Contracts\RegionRepositoryContract;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class RegionController extends Controller
{
    private $_regionRepository;


    public function __construct(RegionRepositoryContract $_regionRepository)
    {
        $this->_regionRepository = $_regionRepository;
    }

    public function villageAjax(Request $request): JsonResponse
    {
        $data = $this->_regionRepository->villages(null, true, true, $request->get('search') ?? '');

        if ($data instanceof LengthAwarePaginator) {
            $container = [];

            foreach ($data->items() as $item) {
                $selected = $request->has('selected') && $request->get('selected') != '' ? $request->get('selected') : null;
                $result = [
                    'id' => $item->id,
                    'text' => $item->name,
                    'selected' => $selected != null && $selected == $item->id,
                    'disabled' => false
                ];

                $container['results'][] = $result;
            }

            $container['pagination']['more'] = $data->hasMorePages();

            return response()->json($container);
        }

        return response()->json([]);
    }

    public function villageAjaxSingle($villageId)
    {
        $data = $this->_regionRepository->villages(null, true, true, '', $villageId);
        if ($data instanceof Model) return response()->json($data);
        return response()->json([]);
    }
}
