<?php


namespace App\Transformers\Order;


use App\Models\Order;
use League\Fractal\TransformerAbstract;

class OrderTransformer extends TransformerAbstract
{
    public function transform(Order $order): array
    {
        return [
            'id' => $order->id,
            'code' => $order->code,
            'sender_name' => $order->sender_name,
            'sender_phone' => $order->sender_phone,
            'sender_address' => $order->sender_address,
            'receiver_name' => $order->receiver_name,
            'receiver_phone' => $order->receiver_phone,
            'receiver_address' => $order->receiver_address,
            'warehouse_origin' => $order->warehouse_origin,
            'warehouse_destination' => $order->warehouse_destination,
            'status' => $order->status,
        ];
    }
}