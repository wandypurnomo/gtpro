<?php


namespace App\Transformers;


use Illuminate\Support\Collection;
use League\Fractal\TransformerAbstract;

class AuthTransformer extends TransformerAbstract
{
    public function transform(Collection $collection): array
    {
        return [
            'token' => $collection['token']
        ];
    }
}