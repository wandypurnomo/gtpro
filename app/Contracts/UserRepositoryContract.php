<?php


namespace App\Contracts;


use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

interface UserRepositoryContract
{
    public function register(array $data): Model;

    public function updateProfile(array $data, string $userId): Model;

    public function store(array $data): Model;

    public function update(array $data, string $userId): Model;

    public function allUser(Request $request, int $perPage = 10): LengthAwarePaginator;

    public function show(string $userId): Model;

    public function delete(string $userId): void;
}