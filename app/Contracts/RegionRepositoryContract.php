<?php


namespace App\Contracts;


use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Collection;

interface RegionRepositoryContract
{
    public function provinces(): Collection;

    public function regencies(?int $provinceId): Collection;

    public function districts(?int $regencyId): Collection;

    /**
     * @param int|null $districtId
     * @param bool $full
     * @param bool $paginated
     * @param string $search
     * @param string $villageId
     * @return Collection|LengthAwarePaginator
     */
    public function villages(?int $districtId, bool $full = false, bool $paginated = false, string $search = '', string $villageId = '');
}
