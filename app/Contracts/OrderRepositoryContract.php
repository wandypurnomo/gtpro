<?php


namespace App\Contracts;


use App\Models\Agent;
use App\Models\Order;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

interface OrderRepositoryContract
{
    public function __construct(Order $_model);

    /**
     * @param array $data
     * @param Model|Agent $agent
     * @return Model
     */
    public function create(array $data, Agent $agent): Model;

    /**
     * @param Request $request
     * @param int $perPage
     * @return LengthAwarePaginator
     */
    public function all(Request $request, int $perPage = 10): LengthAwarePaginator;

    /**
     * @param Request $request
     * @param Model|Agent $agent
     * @param int $perPage
     * @return LengthAwarePaginator
     */
    public function allByAgent(Request $request, Agent $agent, int $perPage = 10): LengthAwarePaginator;

    /**
     * @param string $orderId
     * @return Model
     */
    public function show(string $orderId): Model;
}