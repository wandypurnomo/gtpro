<?php


namespace App\Contracts;


use App\Models\Agent;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

interface PickUpRepositoryContract
{
    /**
     * @param Request $request
     * @param int $perPage
     * @return LengthAwarePaginator
     */
    public function allPickUp(Request $request, int $perPage = 10): LengthAwarePaginator;

    /**
     * @param string $pickUpId
     * @return Model
     */
    public function show(string $pickUpId): Model;

    /**
     * @param $data
     * @param Model|Agent $agent
     * @return Model
     */
    public function requestPickup($data, Agent $agent): Model;

    /**
     * @param int $status
     * @param string $pickUpId
     * @return Model
     */
    public function updatePickUpStatus(int $status, string $pickUpId): Model;

    /**
     * @param array $data
     * @param string $pickUpId
     * @return Model
     */
    public function update(array $data, string $pickUpId): Model;

    /**
     * @param string $pickUpId
     */
    public function delete(string $pickUpId): void;

    /**
     * @param array $fleetIds
     * @param string $pickUpId
     */
    public function assignFleetsToPickUp(array $fleetIds, string $pickUpId): void;
}