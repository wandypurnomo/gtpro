<?php


namespace App\Contracts;


use App\Models\Agent;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

interface AgentRepositoryContract
{
    public function allAgent(Request $request, int $perPage = 10): LengthAwarePaginator;

    public function store(array $data): Model;

    public function update(array $data, string $id): Model;

    public function delete(string $agentId): void;

    /**
     * @param string $agentId
     * @return Model|Agent
     */
    public function show(string $agentId): Model;

    /**
     * @param Authenticatable|Model $user
     * @return Model
     */
    public function showAgentByUser(Model $user): Model;
}