<?php


namespace App\Repositories;


use App\Contracts\OrderRepositoryContract;
use App\Events\User\OrderCreated;
use App\Models\Agent;
use App\Models\Order;
use Closure;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class OrderRepository implements OrderRepositoryContract
{
    private $_model;

    public function __construct(Order $_model)
    {
        $this->_model = $_model;
    }

    public function create(array $data, Agent $agent): Model
    {
        $data = $agent->orders()->create($data);
        event(new OrderCreated($data));
        return $data;
    }

    public function all(Request $request, int $perPage = 10): LengthAwarePaginator
    {
        $model = $this->_model->newQuery();
        $model->where($this->_whereCondition($request));
        $model->orderBy('created_at', 'asc');
        return $model->paginate($perPage);
    }

    private function _whereCondition(Request $request): Closure
    {
        return function (Builder $builder) use ($request) {
            if ($request->has('code') && $request->get('code') != '') {
                $builder->where('code', $request->get('code'));
            }

            if ($request->has('origin') && $request->get('origin') != '') {
                $builder->where('warehouse_origin', $request->get('origin'));
            }

            if ($request->has('destination') && $request->get('destination') != '') {
                $builder->where('warehouse_destination', $request->get('warehouse_destination'));
            }

            if ($request->has('type') && $request->get('type') != '') {
                $builder->where('goods_type', $request->get('type'));
            }

            if ($request->has('sender') && $request->get('sender') != '') {
                $builder->where('sender_name', 'like', '%' . $request->get('sender') . '%');
            }

            if ($request->has('receiver') && $request->get('receiver') != '') {
                $builder->where('receiver_name', 'like', '%' . $request->get('receiver') . '%');
            }

            if ($request->has('status') && $request->get('status') != '') {
                $builder->where('status', $request->get('status'));
            }
        };
    }

    public function allByAgent(Request $request, Agent $agent, int $perPage = 10): LengthAwarePaginator
    {
        $model = $agent->orders();
        $model->where($this->_whereCondition($request));
        $model->orderBy('created_at', 'asc');
        return $model->paginate($perPage);
    }

    public function show(string $orderId): Model
    {
        return $this->_model->findOrFail($orderId);
    }
}