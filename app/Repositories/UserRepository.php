<?php


namespace App\Repositories;


use App\Contracts\UserRepositoryContract;
use App\Events\User\UserRegistered;
use App\Events\User\UserStored;
use App\Events\User\UserUpdateProfile;
use App\Models\User;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;

class UserRepository implements UserRepositoryContract
{
    private $_model;

    public function __construct(User $_model)
    {
        $this->_model = $_model;
    }

    public function register(array $data): Model
    {
        $user = $this->_model->firstOrCreate($data);
        event(new UserRegistered($user));
        return $user;
    }

    public function updateProfile(array $data, string $userId): Model
    {
        $user = $this->_model->findOrFail($userId);
        $user->update($data);
        event(new UserUpdateProfile($user));
        return $user;
    }

    public function store(array $data): Model
    {
        $roles = isset($data['roles']) ? $data['roles'] : [];
        unset($data['roles']);

        $user = $this->_model->firstOrCreate($data);
        event(new UserStored($user));

        if (count($roles) > 0) {
            foreach ($roles as $role) {
                $role = Role::findOrFail($role);
                $user->assignRole($role);
            }
        }

        return $user;
    }

    public function update(array $data, $userId): Model
    {
        $user = $this->show($userId);
        $roles = isset($data['roles']) ? $data['roles'] : [];
        unset($data['roles']);

        $user->update($data);

        if (count($roles) > 0) {
            $user->syncRoles($roles);
        } else {
            $user->roles()->delete();
        }

        return $user;
    }

    public function show(string $userId): Model
    {
        return $this->_model->findOrFail($userId);
    }

    public function allUser(Request $request, int $perPage = 10): LengthAwarePaginator
    {
        $where = function (Builder $builder) use ($request) {
            if ($request->has('kwd') && $request->get('kwd') != '') {
                $builder
                    ->where('name', 'like', '%' . $request->get('kwd') . '%')
                    ->orWhere('email', 'like', '%' . $request->get('kwd') . '%');
            }
        };

        $query = $this->_model->newQuery();
        $query->where($where);
        $query->orderBy('created_at', 'asc');
        return $query->paginate($perPage);
    }

    public function delete(string $userId): void
    {
        $user = $this->show($userId);
        $user->syncRoles([]);
        $user->delete();
    }
}