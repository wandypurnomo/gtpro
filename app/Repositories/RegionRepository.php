<?php


namespace App\Repositories;


use App\Contracts\RegionRepositoryContract;
use App\Models\District;
use App\Models\Province;
use App\Models\Regency;
use App\Models\Village;
use Illuminate\Database\Eloquent\Collection;

class RegionRepository implements RegionRepositoryContract
{
    private $_provinceModel;
    private $_regencyModel;
    private $_districtModel;
    private $_villageModel;

    public function __construct(
        Province $_provinceModel,
        Regency $_regencyModel,
        District $_districtModel,
        Village $_villageModel
    )
    {
        $this->_provinceModel = $_provinceModel;
        $this->_regencyModel = $_regencyModel;
        $this->_districtModel = $_districtModel;
        $this->_villageModel = $_villageModel;
    }


    public function provinces(): Collection
    {
        return $this->_provinceModel->newQuery()->orderBy("name", "asc")->get();
    }

    public function regencies(?int $provinceId): Collection
    {
        $model = $this->_regencyModel->newQuery();

        if ($provinceId != null) {
            $model->where("province_id", $provinceId);
        }

        $model->orderBy("name", "asc");

        return $model->get();
    }

    public function districts(?int $regencyId): Collection
    {
        $model = $this->_districtModel->newQuery();

        if ($regencyId != null) {
            $model->where("regency_id", $regencyId);
        }

        $model->orderBy("name", "asc");

        return $model->get();
    }

    public function villages(?int $districtId, bool $full = false, bool $paginated = false, string $search = '', string $villageId = '')
    {
        $model = $this->_villageModel->newQuery();

        if ($full) {
            $model->selectRaw('CONCAT_WS(" - ",villages.name,districts.name,regencies.name,provinces.name) as name,villages.id');
            $model->join('districts', 'districts.id', '=', 'villages.district_id');
            $model->join('regencies', 'regencies.id', '=', 'districts.regency_id');
            $model->join('provinces', 'provinces.id', '=', 'regencies.province_id');
        }

        if ($districtId != null) {
            $model->where("district_id", $districtId);
        }

        if ($search != '') {
            $str = '%' . $search . '%';
            $model->whereRaw('CONCAT_WS(" - ",villages.name,districts.name,regencies.name,provinces.name) like "' . $str . '"');
        }

        $model->orderBy("name", "asc");

        if ($villageId != null) return $model->where('villages.id', $villageId)->first();
        if ($paginated) return $model->paginate(10);
        return $model->get();
    }
}
