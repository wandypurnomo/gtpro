<?php


namespace App\Repositories;


use App\Contracts\AgentRepositoryContract;
use App\Models\Agent;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class AgentRepository implements AgentRepositoryContract
{
    private $_model;

    public function __construct(Agent $_model)
    {
        $this->_model = $_model;
    }

    public function allAgent(Request $request, int $perPage = 10): LengthAwarePaginator
    {
        $query = $this->_model->newQuery();
        $where = function (Builder $builder) use ($request) {
            if ($request->has('kwd') && $request->get('kwd') != '') {
                $builder
                    ->where('code', 'like', '%' . $request->get('kwd') . '%')
                    ->orWhere('name', 'like', '%' . $request->get('kwd') . '%')
                    ->orWhere('address', 'like', '%' . $request->get('kwd') . '%');
            }
        };

        $query->where($where);
        $query->orderBy('created_at', 'asc');
        return $query->paginate($perPage);
    }

    public function store(array $data): Model
    {
        return $this->_model->create($data);
    }

    public function update(array $data, string $id): Model
    {
        $model = $this->show($id);
        $model->update($data);
        return $model;
    }

    public function show(string $agentId): Model
    {
        return $this->_model->findOrFail($agentId);
    }

    public function delete(string $agentId): void
    {
        $this->show($agentId)->delete();
    }

    public function showAgentByUser(Model $user): Model
    {
        return $user->agents()->firstOrFail();
    }
}