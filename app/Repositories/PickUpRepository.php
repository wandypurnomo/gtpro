<?php


namespace App\Repositories;


use App\Contracts\PickUpRepositoryContract;
use App\Events\User\DriverAssigned;
use App\Events\User\PickUpRequestCreated;
use App\Models\Agent;
use App\Models\PickUp;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class PickUpRepository implements PickUpRepositoryContract
{
    private $_model;

    public function __construct(PickUp $_model)
    {
        $this->_model = $_model;
    }

    public function allPickUp(Request $request, int $perPage = 10): LengthAwarePaginator
    {
        $query = $this->_model->newQuery();
        $where = function (Builder $builder) use ($request) {
            if ($request->has('kwd') && $request->get('kwd') != '') {
                $builder->whereHas('agent', function (Builder $agent) use ($request) {
                    $agent
                        ->where('code', 'like', '%' . $request->get('kwd') . '%')
                        ->orWhere('name', 'like', '%' . $request->get('kwd') . '%')
                        ->orWhere('address', 'like', '%' . $request->get('kwd') . '%');
                });
            }

            if ($request->has('status') && $request->get('status') != '') {
                $builder->where('status', $request->get('status'));
            }

            if ($request->has('preference') && $request->get('preference') != '') {
                $builder->where('pick_up_preference', $request->get('preference'));
            }
        };

        $query->where($where);
        $query->orderBy('created_at', 'asc');
        return $query->paginate($perPage);
    }

    public function requestPickup($data, Agent $agent): Model
    {
        $data = $agent->pick_ups()->create($data);
        event(new PickUpRequestCreated($data));
        return $data;
    }

    public function updatePickUpStatus(int $status, string $pickUpId): Model
    {
        $data = $this->show($pickUpId);
        $data->update(['status' => $status]);
        return $data;
    }

    public function show(string $pickUpId): Model
    {
        return $this->_model->findOrFail($pickUpId);
    }

    public function update(array $data, string $pickUpId): Model
    {
        $model = $this->show($pickUpId);
        $model->update($data);
        return $model;
    }

    public function delete(string $pickUpId): void
    {
        $this->show($pickUpId)->delete();
    }

    public function assignFleetsToPickUp(array $fleetIds, string $pickUpId): void
    {
        $pickUp = $this->show($pickUpId);
        $pickUp->fleets()->sync($fleetIds);
        event(new DriverAssigned($pickUp));
    }
}