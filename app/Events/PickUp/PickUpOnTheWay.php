<?php

namespace App\Events\User;

use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class PickUpOnTheWay
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $pickUp;

    /**
     * Create a new event instance.
     *
     * @param Model $pickUp
     */
    public function __construct(Model $pickUp)
    {
        $this->pickUp = $pickUp;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
