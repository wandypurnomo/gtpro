<?php

namespace App\Models;

use App\Constants\PickupStatus;
use App\Events\User\PickUpOnTheWay;
use App\Events\User\PickUpPickedUp;
use Eloquent;
use Envant\Fireable\FireableAttributes;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;
use Wandxx\Support\Traits\UuidForKey;

/**
 * App\Models\PickUp
 *
 * @property string $id
 * @property string $agent_id
 * @property int $status
 * @property int $pick_up_preference
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property-read Agent $agent
 * @property-read Collection|Warrant[] $warrants
 * @property-read int|null $warrants_count
 * @method static Builder|PickUp newModelQuery()
 * @method static Builder|PickUp newQuery()
 * @method static Builder|PickUp query()
 * @method static Builder|PickUp whereAgentId($value)
 * @method static Builder|PickUp whereCreatedAt($value)
 * @method static Builder|PickUp whereId($value)
 * @method static Builder|PickUp wherePickUpPreference($value)
 * @method static Builder|PickUp whereStatus($value)
 * @method static Builder|PickUp whereUpdatedAt($value)
 * @mixin Eloquent
 * @property int $fleet_requirement
 * @property-read Collection|Fleet[] $fleets
 * @property-read int|null $fleets_count
 * @method static Builder|PickUp whereFleetRequirement($value)
 */
class PickUp extends Model
{
    use UuidForKey, FireableAttributes;

    public $incrementing = false;
    protected $fillable = ['agent_id', 'status', 'pick_up_preference', 'fleet_requirement'];
    protected $fireableAttributes = [
        'status' => [
            PickupStatus::ON_THE_WAY => PickUpOnTheWay::class,
            PickupStatus::PICKED_UP => PickUpPickedUp::class,
        ]
    ];

    public function agent()
    {
        return $this->belongsTo(Agent::class);
    }

    public function fleets()
    {
        return $this->belongsToMany(Fleet::class);
    }

    public function warrants()
    {
        return $this->hasMany(Warrant::class);
    }
}
