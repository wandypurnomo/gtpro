<?php

namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\District
 *
 * @property int $id
 * @property int $regency_id
 * @property string $name
 * @property-read Regency $regency
 * @property-read Collection|Village[] $villages
 * @property-read int|null $villages_count
 * @method static Builder|District newModelQuery()
 * @method static Builder|District newQuery()
 * @method static Builder|District query()
 * @method static Builder|District whereId($value)
 * @method static Builder|District whereName($value)
 * @method static Builder|District whereRegencyId($value)
 * @mixin Eloquent
 */
class District extends Model
{
    public $incrementing = false;
    public $timestamps = false;
    protected $guarded = ["id"];
    protected $casts = [
        "id" => "int",
        "regency_id" => "int"
    ];

    public function regency()
    {
        return $this->belongsTo(Regency::class);
    }

    public function villages()
    {
        return $this->hasMany(Village::class);
    }
}
