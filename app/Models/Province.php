<?php

namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Province
 *
 * @property int $id
 * @property string $name
 * @property-read Collection|Regency[] $regencies
 * @property-read int|null $regencies_count
 * @method static Builder|Province newModelQuery()
 * @method static Builder|Province newQuery()
 * @method static Builder|Province query()
 * @method static Builder|Province whereId($value)
 * @method static Builder|Province whereName($value)
 * @mixin Eloquent
 */
class Province extends Model
{
    public $incrementing = false;
    public $timestamps = false;
    protected $guarded = ["id"];
    protected $casts = [
        "id" => "int"
    ];

    public function regencies()
    {
        return $this->hasMany(Regency::class);
    }
}
