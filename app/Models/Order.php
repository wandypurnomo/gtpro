<?php

namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;
use Wandxx\Support\Traits\UuidForKey;

/**
 * App\Models\Order
 *
 * @property string $id
 * @property string $agent_id
 * @property string $code
 * @property string $warehouse_origin
 * @property string $warehouse_destination
 * @property int $goods_type
 * @property float $weight
 * @property string $notes
 * @property string $sender_name
 * @property string $sender_phone
 * @property string|null $sender_phone2
 * @property string $sender_address
 * @property string $receiver_name
 * @property string $receiver_phone
 * @property string|null $receiver_phone2
 * @property string $receiver_address
 * @property int $status
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property-read Agent $agent
 * @method static Builder|Order newModelQuery()
 * @method static Builder|Order newQuery()
 * @method static Builder|Order query()
 * @method static Builder|Order whereAgentId($value)
 * @method static Builder|Order whereCode($value)
 * @method static Builder|Order whereCreatedAt($value)
 * @method static Builder|Order whereGoodsType($value)
 * @method static Builder|Order whereId($value)
 * @method static Builder|Order whereNotes($value)
 * @method static Builder|Order whereReceiverAddress($value)
 * @method static Builder|Order whereReceiverName($value)
 * @method static Builder|Order whereReceiverPhone($value)
 * @method static Builder|Order whereReceiverPhone2($value)
 * @method static Builder|Order whereSenderAddress($value)
 * @method static Builder|Order whereSenderName($value)
 * @method static Builder|Order whereSenderPhone($value)
 * @method static Builder|Order whereSenderPhone2($value)
 * @method static Builder|Order whereStatus($value)
 * @method static Builder|Order whereUpdatedAt($value)
 * @method static Builder|Order whereWarehouseDestination($value)
 * @method static Builder|Order whereWarehouseOrigin($value)
 * @method static Builder|Order whereWeight($value)
 * @mixin Eloquent
 */
class Order extends Model
{
    use UuidForKey;

    public $incrementing = false;
    protected $guarded = ['id'];

    protected static function boot()
    {
        parent::boot();

        self::creating(function ($model) {
            $code = config('gtpro.order_prefix');
            $code = $code . Carbon::now()->format('YmdHis') . rand(100, 999);
            $model->code = $code;
        });
    }

    public function agent()
    {
        return $this->belongsTo(Agent::class);
    }
}
