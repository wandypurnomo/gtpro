<?php

namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;
use Wandxx\Support\Traits\UuidForKey;

/**
 * App\Models\Warrant
 *
 * @property string $id
 * @property string $code
 * @property string $fleet_id
 * @property string $pick_up_id
 * @property int $status
 * @property string|null $trouble
 * @property string|null $notes
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property-read Fleet $fleet
 * @property-read PickUp $pick_up
 * @method static Builder|Warrant newModelQuery()
 * @method static Builder|Warrant newQuery()
 * @method static Builder|Warrant query()
 * @method static Builder|Warrant whereCode($value)
 * @method static Builder|Warrant whereCreatedAt($value)
 * @method static Builder|Warrant whereFleetId($value)
 * @method static Builder|Warrant whereId($value)
 * @method static Builder|Warrant whereNotes($value)
 * @method static Builder|Warrant wherePickUpId($value)
 * @method static Builder|Warrant whereStatus($value)
 * @method static Builder|Warrant whereTrouble($value)
 * @method static Builder|Warrant whereUpdatedAt($value)
 * @mixin Eloquent
 */
class Warrant extends Model
{
    use UuidForKey;

    public $incrementing = false;
    protected $fillable = ['code', 'fleet_id', 'pick_up_id', 'status', 'trouble', 'notes'];

    public function fleet()
    {
        return $this->belongsTo(Fleet::class);
    }

    public function pick_up()
    {
        return $this->belongsTo(PickUp::class);
    }
}
