<?php

namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;
use Wandxx\Support\Traits\UuidForKey;

/**
 * App\Models\Fleet
 *
 * @property string $id
 * @property string $code
 * @property string $identifier
 * @property int $type
 * @property string $driver
 * @property float $lat
 * @property float $lng
 * @property bool $is_available
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property-read Collection|Warrant[] $warrants
 * @property-read int|null $warrants_count
 * @method static Builder|Fleet newModelQuery()
 * @method static Builder|Fleet newQuery()
 * @method static Builder|Fleet query()
 * @method static Builder|Fleet whereCode($value)
 * @method static Builder|Fleet whereCreatedAt($value)
 * @method static Builder|Fleet whereDriver($value)
 * @method static Builder|Fleet whereId($value)
 * @method static Builder|Fleet whereIdentifier($value)
 * @method static Builder|Fleet whereIsAvailable($value)
 * @method static Builder|Fleet whereLat($value)
 * @method static Builder|Fleet whereLng($value)
 * @method static Builder|Fleet whereType($value)
 * @method static Builder|Fleet whereUpdatedAt($value)
 * @mixin Eloquent
 */
class Fleet extends Model
{
    use UuidForKey;

    public $incrementing = false;
    protected $fillable = ['code', 'identifier', 'type', 'driver', 'lat', 'lng', 'is_available'];
    protected $casts = [
        'is_available' => 'boolean'
    ];

    public function warrants()
    {
        return $this->hasMany(Warrant::class);
    }
}
