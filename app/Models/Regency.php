<?php

namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Regency
 *
 * @property int $id
 * @property int $province_id
 * @property string $name
 * @property-read Collection|District[] $districts
 * @property-read int|null $districts_count
 * @property-read Province $province
 * @method static Builder|Regency newModelQuery()
 * @method static Builder|Regency newQuery()
 * @method static Builder|Regency query()
 * @method static Builder|Regency whereId($value)
 * @method static Builder|Regency whereName($value)
 * @method static Builder|Regency whereProvinceId($value)
 * @mixin Eloquent
 */
class Regency extends Model
{
    public $incrementing = false;
    public $timestamps = false;
    protected $guarded = ["id"];
    protected $casts = [
        "id" => "int",
        "province_id" => "int"
    ];

    public function province()
    {
        return $this->belongsTo(Province::class);
    }

    public function districts()
    {
        return $this->hasMany(District::class);
    }
}
