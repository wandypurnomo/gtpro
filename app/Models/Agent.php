<?php

namespace App\Models;

use App\Constants\AgentType;
use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;
use Wandxx\Support\Traits\UuidForKey;

/**
 * App\Models\Agent
 *
 * @property string $id
 * @property string $code
 * @property int|null $type
 * @property string $name
 * @property string $address
 * @property float|null $lat
 * @property float|null $lng
 * @property string|null $district_id
 * @property string $phone
 * @property string|null $phone2
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property-read mixed $type_text
 * @property-read Collection|Order[] $orders
 * @property-read int|null $orders_count
 * @property-read Collection|User[] $users
 * @property-read int|null $users_count
 * @method static Builder|Agent newModelQuery()
 * @method static Builder|Agent newQuery()
 * @method static Builder|Agent query()
 * @method static Builder|Agent whereAddress($value)
 * @method static Builder|Agent whereCode($value)
 * @method static Builder|Agent whereCreatedAt($value)
 * @method static Builder|Agent whereDistrictId($value)
 * @method static Builder|Agent whereId($value)
 * @method static Builder|Agent whereLat($value)
 * @method static Builder|Agent whereLng($value)
 * @method static Builder|Agent whereName($value)
 * @method static Builder|Agent wherePhone($value)
 * @method static Builder|Agent wherePhone2($value)
 * @method static Builder|Agent whereType($value)
 * @method static Builder|Agent whereUpdatedAt($value)
 * @mixin Eloquent
 * @property-read District|null $district
 * @property-read Collection|PickUp[] $pick_ups
 * @property-read int|null $pick_ups_count
 * @property string|null $village_id
 * @method static Builder|Agent whereVillageId($value)
 */
class Agent extends Model
{
    use UuidForKey;
    public $incrementing = false;
    protected $fillable = [
        'code', 'name', 'address',
        'village_id', 'phone', 'phone2',
        'lat', 'lng', 'type'
    ];
    protected $appends = ['type_text'];

    public function users()
    {
        return $this->belongsToMany(User::class)->withPivot(['status']);
    }

    public function couriers()
    {
        return $this->belongsToMany(User::class, 'agent_courier', 'agent_id', 'courier_id');
    }

    public function drivers()
    {
        return $this->belongsToMany(User::class, 'agent_driver', 'agent_id', 'driver_id');
    }

    public function village()
    {
        return $this->belongsTo(Village::class);
    }

    public function orders()
    {
        return $this->hasMany(Order::class);
    }

    public function pick_ups()
    {
        return $this->hasMany(PickUp::class);
    }

    public function getTypeTextAttribute()
    {
        return !is_null($this->type) ? AgentType::label($this->type) : '-';
    }
}
