<?php

namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Village
 *
 * @property int $id
 * @property string $district_id
 * @property string $name
 * @property-read District $district
 * @method static Builder|Village newModelQuery()
 * @method static Builder|Village newQuery()
 * @method static Builder|Village query()
 * @method static Builder|Village whereDistrictId($value)
 * @method static Builder|Village whereId($value)
 * @method static Builder|Village whereName($value)
 * @mixin Eloquent
 */
class Village extends Model
{
    public $incrementing = false;
    public $timestamps = false;
    protected $guarded = ["id"];

    protected $casts = ["id" => "int"];

    public function district()
    {
        return $this->belongsTo(District::class);
    }
}
