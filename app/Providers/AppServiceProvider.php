<?php

namespace App\Providers;

use App\Contracts\AgentRepositoryContract;
use App\Contracts\PickUpRepositoryContract;
use App\Contracts\RegionRepositoryContract;
use App\Contracts\UserRepositoryContract;
use App\Repositories\AgentRepository;
use App\Repositories\PickUpRepository;
use App\Repositories\RegionRepository;
use App\Repositories\UserRepository;
use Illuminate\Support\ServiceProvider;
use Menu;
use Spatie\Menu\Laravel\Link;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->_registerRepository();
        $this->_buildMenu();
    }

    private function _registerRepository(): void
    {
        $this->app->bind(UserRepositoryContract::class, UserRepository::class);
        $this->app->bind(AgentRepositoryContract::class, AgentRepository::class);
        $this->app->bind(PickUpRepositoryContract::class, PickUpRepository::class);
        $this->app->bind(RegionRepositoryContract::class, RegionRepository::class);
    }

    private function _buildMenu(): void
    {
        Menu::macro('main', function () {
            return Menu::new()
                ->addClass('sidebar-menu')
                ->setAttribute('data-widget', 'tree')
                ->add(Link::toRoute('dashboard', '<i class="fa fa-dashboard"></i><span>Dashboard</span>'))
                ->addIf(auth()->user()->hasRole('super user'), Link::toRoute('usermanager.index', '<i class="fa fa-user-secret"></i><span>User Manager</span>'))
                ->addIf(auth()->user()->hasRole('super user'), Link::toRoute('agentmanager.index', '<i class="fa fa-building"></i><span>Agent Manager</span>'))
                ->addIf(auth()->user()->hasRole('super user'), Link::toRoute('agentmanager.index', '<i class="fa fa-building"></i><span>Agent Manager</span>'))
                ->addIf(auth()->user()->hasRole('agent_warehouse'), Link::toRoute('drivermanager.index', '<i class="fa fa-car"></i><span>Driver Manager</span>'));
        });
    }
}
