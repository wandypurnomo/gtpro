<?php


namespace App\Constants;


use Wandxx\Support\Interfaces\ConstantInterface;
use Wandxx\Support\Traits\HasLabel;

class OrderStatus implements ConstantInterface
{
    use HasLabel;

    const PLACED = 1;
    const SORTING_AT_WAREHOUSE = 2;
    const ON_THE_WAY = 3;
    const TRANSIT = 4;
    const AT_DESTINATION_WAREHOUSE = 5;
    const ON_COURIER = 6;
    const DELIVERED = 7;
    const BACK_TO_WAREHOUSE = 8;

    public static function labels(): array
    {
        return [
            self::PLACED => 'PLACED',
            self::SORTING_AT_WAREHOUSE => 'SORTING AT WAREHOUSE',
            self::ON_THE_WAY => 'ON THE WAY',
            self::TRANSIT => 'TRANSIT',
            self::AT_DESTINATION_WAREHOUSE => 'AT DESTINATION WAREHOUSE',
            self::ON_COURIER => 'ON COURIER',
            self::DELIVERED => 'DELIVERED',
            self::BACK_TO_WAREHOUSE => 'BACK TO WAREHOUSE'
        ];
    }
}