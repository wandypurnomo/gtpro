<?php


namespace App\Constants;


use Wandxx\Support\Interfaces\ConstantInterface;
use Wandxx\Support\Traits\HasLabel;

class WarrantStatus implements ConstantInterface
{
    use HasLabel;

    const PLACED = 1;
    const ON_THE_WAY = 2;
    const PICKED_UP = 3;
    const TRANSIT = 4;
    const DELIVERED = 5;
    const TROUBLE = 6;

    public static function labels(): array
    {
        return [
            self::PLACED => 'PLACED',
            self::ON_THE_WAY => 'ON THE WAY',
            self::PICKED_UP => 'PICKED UP',
            self::TRANSIT => 'TRANSIT',
            self::DELIVERED => 'DELIVERED',
            self::TROUBLE => 'TROUBLE'
        ];
    }
}