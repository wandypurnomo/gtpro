<?php


namespace App\Constants;


use Wandxx\Support\Interfaces\ConstantInterface;
use Wandxx\Support\Traits\HasLabel;

class PickUpPreference implements ConstantInterface
{
    use HasLabel;

    const TRUCK = 1;
    const CAR = 2;
    const MOTORCYCLE = 3;
    const COURIER = 4;

    public static function labels(): array
    {
        return [
            self::TRUCK => 'TRUCK',
            self::CAR => 'CAR',
            self::MOTORCYCLE => 'MOTORCYCLE',
            self::COURIER => 'COURIER',
        ];
    }
}