<?php


namespace App\Constants;


use Wandxx\Support\Interfaces\ConstantInterface;
use Wandxx\Support\Traits\HasLabel;

class PickupStatus implements ConstantInterface
{
    use HasLabel;

    const REQUEST = 1;
    const PLACED = 2;
    const ON_THE_WAY = 3;
    const PICKED_UP = 4;

    public static function labels(): array
    {
        return [
            self::REQUEST => 'REQUEST',
            self::PLACED => 'PLACED',
            self::ON_THE_WAY => 'ON THE WAY',
            self::PICKED_UP => 'PICKED UP'
        ];
    }
}