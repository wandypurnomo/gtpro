<?php


namespace App\Constants;


use Wandxx\Support\Interfaces\ConstantInterface;
use Wandxx\Support\Traits\HasLabel;

class AgentType implements ConstantInterface
{
    use HasLabel;

    const REGULAR = 1;
    const WAREHOUSE = 2;

    public static function labels(): array
    {
        return [
            self::REGULAR => 'REGULAR',
            self::WAREHOUSE => 'WAREHOUSE'
        ];
    }
}